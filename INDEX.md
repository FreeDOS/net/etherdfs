# ETHERDFS

EtherDFS is an 'installable filesystem' TSR for DOS. It maps a drive from a remote computer (typically Linux-based) to a local drive letter, using raw ethernet frames to communicate.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## ETHERDFS.LSM

<table>
<tr><td>title</td><td>ETHERDFS</td></tr>
<tr><td>version</td><td>0.8.3</td></tr>
<tr><td>entered&nbsp;date</td><td>2023-06-22</td></tr>
<tr><td>description</td><td>Ethernet DOS file system</td></tr>
<tr><td>summary</td><td>EtherDFS is an 'installable filesystem' TSR for DOS. It maps a drive from a remote computer (typically Linux-based) to a local drive letter, using raw ethernet frames to communicate.</td></tr>
<tr><td>keywords</td><td>etherdfs</td></tr>
<tr><td>author</td><td>Mateusz Viste</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://etherdfs.sourceforge.net</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[MIT License](LICENSE)</td></tr>
</table>
